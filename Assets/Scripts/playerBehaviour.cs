﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class playerBehaviour : MonoBehaviour {

	private float distanceToCamera;

	[SerializeField]
	private Text countPoints;

	[SerializeField]
	private Text gameOverText;

	[SerializeField]
	private Button tryAgainButton;

	private int totalPoints;

	[SerializeField]
	private float jumpValue = 10.0f;

	private bool touchesGround = true;

	private Rigidbody rigidBody;
	// Use this for initialization
	void Start () {
		rigidBody = GetComponent<Rigidbody>();
		distanceToCamera = (transform.position - Camera.main.transform.position).magnitude;
		StartGame ();
	}

	void StartGame(){
		totalPoints = 0;
		UpdateScoreText();
		gameOverText.text = "";
		HideTryAgainButton ();
		Camera.main.GetComponent<sphereRain> ().RemoveFallingSpheres ();
		Camera.main.GetComponent<sphereRain>().Spawn ();
	}

	void HideTryAgainButton(){
			tryAgainButton.enabled = false;
			tryAgainButton.GetComponentInChildren<CanvasRenderer> ().SetAlpha (0);
			tryAgainButton.GetComponentInChildren<Text> ().color = Color.clear;
			tryAgainButton.onClick.RemoveAllListeners ();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0)) {
			if (canJump ()) {
				rigidBody.AddForce(new Vector3(0, jumpValue, 0), ForceMode.Impulse);
				touchesGround = false;
			}
			else {
				Debug.Log ("cant jump!");
			}
		}
		Vector3 mouseLocation = Input.mousePosition;
		mouseLocation.z = distanceToCamera;
		Vector3 newPosition = Camera.main.ScreenToWorldPoint (mouseLocation);
		newPosition.y = transform.position.y;
		newPosition.z = transform.position.z;
		transform.position = newPosition;
	}

	void OnCollisionEnter(Collision collision){
		Debug.Log("collided ");

		if  (collidedWithGround(collision)){
			Debug.Log("with ground");
			touchesGround = true;
			if (totalPoints>0){
				EndGame();
			}
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.CompareTag ("Sphere")) {
			Debug.Log("triggered sphere" + rigidBody.velocity.ToString());
			RemoveCurrentForce();
			rigidBody.AddForce(new Vector3(0, jumpValue, 0), ForceMode.Impulse);
			sphereRain.sphereCount --;
			other.gameObject.SetActive(false);
			totalPoints += 10;
			UpdateScoreText();
		}
	}

	bool collidedWithGround(Collision collision){
		return (collision.gameObject.name == "Ground Object");
	}

	bool canJump(){
		return (totalPoints == 0 && touchesGround);
	}

	void UpdateScoreText(){
		countPoints.text = "Score: " + totalPoints.ToString ();
	}

	void EndGame(){
		gameOverText.text = "Game Over! Final Score: " + totalPoints.ToString ();
		ShowTryAgainButton ();
	}

	void ShowTryAgainButton(){
		tryAgainButton.enabled = true;
		tryAgainButton.GetComponentInChildren<CanvasRenderer>().SetAlpha(1);
		tryAgainButton.GetComponentInChildren<Text>().color = Color.black;
		tryAgainButton.onClick.AddListener (() => {
			StartGame();
		});
	}

	void RemoveCurrentForce(){
		rigidBody.velocity = Vector3.zero;
		rigidBody.angularVelocity = Vector3.zero;
	}
}
