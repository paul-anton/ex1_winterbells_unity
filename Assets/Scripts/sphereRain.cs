﻿using UnityEngine;
using System.Collections;

public class sphereRain : MonoBehaviour {

	[SerializeField]
	private float fixedInterval = 5.0f;

	[SerializeField]
	private float delay = 0.3f;

	[SerializeField]
	private GameObject sphere;

	private GameObject lastSphere;

	[SerializeField]
	private GameObject cubePlayer;

	public static int sphereCount;

	private int maxSpheres = 6;

	void Start () {
		sphereCount = 0;
		InvokeRepeating ("Spawn", delay, delay);
	}

	public void Spawn () {
		if (sphereCount < maxSpheres) {
			lastSphere = (GameObject)Instantiate (sphere, new Vector3 (Random.Range (-4, 4), calculateY () + fixedInterval, 0), Quaternion.identity);
			sphereCount ++;
		}
	}

	float calculateY(){
		float yPos;
		if (lastSphere == null) {
			yPos = cubePlayer.transform.position.y;
		} else {
			yPos = lastSphere.transform.position.y;
		}
		return yPos;
	}

	public void RemoveFallingSpheres(){
		lastSphere = null;
		sphereCount = 0;
		GameObject[] spheres = GameObject.FindGameObjectsWithTag ("Sphere");
		foreach (GameObject sphere in spheres) {
			Debug.Log("Destroyed ");
			Destroy(sphere.gameObject);
		}
	}
}
