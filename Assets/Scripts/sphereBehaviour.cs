﻿using UnityEngine;
using System.Collections;

public class sphereBehaviour : MonoBehaviour {

	[SerializeField]
	private float maxFallingSpeed = 2.0f;

	[SerializeField]
	private float distToGround = 3.0f;

	private Renderer rendererObj;
	private Rigidbody rigidBody;

	int groundMask = 1 << LayerMask.NameToLayer ("Ground");

	// Use this for initialization
	void Start () {
		rendererObj = GetComponent<Renderer> ();
		rigidBody = GetComponent<Rigidbody> ();
	}

	void SetFixedVelocity(){
		rigidBody.velocity = rigidBody.velocity.normalized * maxFallingSpeed;
	}

	// Update is called once per frame
	void Update(){
		if (isCloseToGround ()) {
			Vanish();
		}
	}

	void FixedUpdate(){
		if (rigidBody.velocity.magnitude > maxFallingSpeed) {
			rigidBody.velocity = rigidBody.velocity.normalized * maxFallingSpeed;
		}
	}

	bool isCloseToGround(){
		return Physics.Raycast (transform.position, -Vector3.up, distToGround, groundMask);
	}

	void Vanish(){
		rendererObj.enabled = false;
		gameObject.SetActive (false);
		Destroy (gameObject);
		sphereRain.sphereCount --;
	}
}
