﻿using UnityEngine;
using System.Collections;

public class basicFollow : MonoBehaviour {

	[SerializeField]
	private GameObject target;

	private Vector3 offset;
	// Use this for initialization
	void Start () {
		offset = transform.position - target.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		Vector3 newPosition = transform.position;
		newPosition.y = target.transform.position.y+ offset.y;
		transform.position = newPosition;
	}
}
